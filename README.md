# README #


### Useful links ###

* [Adafruit Feather HUZZAH ESP8266](https://learn.adafruit.com/adafruit-feather-huzzah-esp8266)
* [Stuff to install](https://drive.google.com/drive/folders/0B8zQMYnlrfaQdGs2SDJlY2Fadlk?usp=sharing)
* [Adafruit TCS34725 Color ](https://learn.adafruit.com/adafruit-color-sensors)
* [Adafruit MiCS5524 CO](https://learn.adafruit.com/adafruit-mics5524-gas-sensor-breakout/downloads?view=all)
* [Adafruit BME280](https://learn.adafruit.com/adafruit-bme280-humidity-barometric-pressure-temperature-sensor-breakout/overview)
* [Adafruit BNO055 ](https://learn.adafruit.com/adafruit-bno055-absolute-orientation-sensor/overview?view=all)